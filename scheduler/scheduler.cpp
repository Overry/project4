/*
 * Problem #56 on page 434 of the textbook.
 */

#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <algorithm>

using namespace std;
//First come first served scheduling
void resolveFCFS(vector<int> cylinders, int initial){
    cout << "FCFS Ordering: " << initial << " ";

    for (int val : cylinders) {
        cout << val << " ";
    }

    cout << endl;

    int distance = 0;
    int prevValue = 0;
    for (int val : cylinders){
        switch(prevValue){
            case 0 : distance += abs(initial - val);
                     break;
            default : distance += abs(prevValue - val);
                      break;
        }
        prevValue = val;
    }

    cout << "FCFS distancd: " << distance <<  endl << endl;
}
//--------------------------------------------------------------
//Shortest seek first scheduling
void resolveSSF(vector<int> cylinders, int initial){
    cout << "SSf Ordering: " << initial << " ";

    int prevValue = initial;
    int tmpStepDistance;
    int stepDistance = 0;
    int distance = 0;
    int heldValue = 0;

    vector<int>::iterator position;

    for (int val : cylinders) {

        position = cylinders.begin();

        for (auto val = cylinders.begin(); val != cylinders.end(); ++val) {

            tmpStepDistance = abs(prevValue - *val);

            //I'm having trouble with this portion of code, depending on the exact configuration
            //it breaks the progression of the scheduler in different ways.
            if((stepDistance == 0 || stepDistance >= tmpStepDistance) && tmpStepDistance != 0){
                stepDistance = tmpStepDistance;
                heldValue = *val;
                position = val;
            }

        }

        prevValue = heldValue;
        distance += stepDistance;

        stepDistance = 0;
        heldValue = 0;

        cylinders.erase(position);

        cout << prevValue << " ";

    }

    cout << endl << "SSF Distance: " << distance << endl << endl;

    }
//--------------------------------------------------------------
//Elevator scheduling
void resolveElevator(vector<int> cylinders, int initial){
    cout << "Elevator Ordering: " << initial << " ";

    int prevValue = initial;
    int tmpStepDistance;
    int stepDistance = 0;
    int distance = 0;
    int heldValue = 0;
    int direction;//direction = 1 means up, direction = 0 means down
    vector<int>::iterator position;

    while (stepDistance == 0){
        for (auto val = cylinders.begin(); val != cylinders.end(); ++val){
            tmpStepDistance = prevValue - *val;

            if(stepDistance == 0 || abs(stepDistance) >= abs(tmpStepDistance)){
                stepDistance = tmpStepDistance;
                heldValue = *val;
                position = val;
            }
        }

        prevValue = heldValue;
        distance += abs(stepDistance);

        heldValue = 0;

        cylinders.erase(position);

        cout << prevValue << " ";
    }
    if (stepDistance < 0){
        direction = 1;
    }else if (stepDistance > 0){
        direction = 0;
    }

    bool goingUp;

    //probably should have two external functions for the ascending and decending scheduling loops
    //but I don't have time to re-tool the code for that
    if(direction = 1){
        for (int i : cylinders){
            stepDistance = 0;
            position = cylinders.begin();
            for (auto val = cylinders.begin(); val != cylinders.end(); ++val){
                goingUp = false;
                if (*val >= prevValue && direction == 1){
                    tmpStepDistance = abs(prevValue - *val);


                    if(stepDistance == 0 || stepDistance >= tmpStepDistance){
                        stepDistance = tmpStepDistance;
                        heldValue = *val;
                        position = val;
                    }

                    goingUp = true;
                }
            }

            //Problems with the elevator scheduler stem from below loop.
            //for rising values, it progresses correctly, for descending values
            //the loop starts alternating directions unexpectedly
            if (goingUp == false){
                direction = 0;
                for (auto val = cylinders.begin(); val != cylinders.end(); ++val){
                    if (*val <= prevValue){
                        tmpStepDistance = abs(prevValue - *val);
                        if(stepDistance == 0 || stepDistance >= tmpStepDistance){
                            stepDistance = tmpStepDistance;
                            heldValue = *val;
                            position = val;
                        }
                    }
                }
            }

            prevValue = heldValue;
            distance += stepDistance;

            cylinders.erase(position);

            cout << prevValue << " ";
        }
    }else if(direction = 0){
        for (int i : cylinders){
            stepDistance = 0;
            position = cylinders.begin();
            for (auto val = cylinders.begin(); val != cylinders.end(); ++val){
                goingUp = true;
                if (*val <= prevValue && direction == 0){
                    tmpStepDistance = abs(prevValue - *val);


                    if(stepDistance == 0 || stepDistance >= tmpStepDistance){
                        stepDistance = tmpStepDistance;
                        heldValue = *val;
                        position = val;
                    }

                    goingUp = false;
                }
            }

            if (goingUp == true){
                direction = 1;
                for (auto val = cylinders.begin(); val != cylinders.end(); ++val){
                    if (*val >= prevValue){
                        tmpStepDistance = abs(prevValue - *val);
                        if(stepDistance == 0 || stepDistance >= tmpStepDistance){
                            stepDistance = tmpStepDistance;
                            heldValue = *val;
                            position = val;
                        }
                    }
                }
            }

            prevValue = heldValue;
            distance += stepDistance;

            cylinders.erase(position);

            cout << prevValue << " ";
        }
    }

    cout << endl << "Elevator distance: " << distance << endl;

}

int main(int argc, char **argv) {
  if (argc < 3) {
    printf("Usage: scheduler i filename\n");
    printf("  i - starting location of cylinder head\n");
    return 1;
  }
  int argi = 1;
  const int startPos = atoi(argv[argi++]);
  const string filename = argv[argi++];

  // Read cylinder requests into cylinders array
  vector<int> cylinders;
  ifstream file(filename);
  int inCylinder;
  file >> inCylinder;
  while (!file.eof()) {
    cylinders.push_back(inCylinder);
    file >> inCylinder;
  }
  file.close();

  // This is where you'll implement FCFS, FFS and elevator.
  cout << "Cylinders to visit: " << startPos << " ";
  for (int val : cylinders) {
    cout << val << " ";
  }
  cout << endl << endl;

  resolveFCFS(cylinders, startPos);
  resolveSSF(cylinders, startPos);
  resolveElevator(cylinders, startPos);
}
